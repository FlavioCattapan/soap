package br.com.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import br.com.entities.Autor;

public class AdaptadorAutores extends XmlAdapter<String, Autor> {

	@Override
	public String marshal(Autor autor) throws Exception {
	return autor.getNome();
	}
	@Override
	public Autor unmarshal(String autor) throws Exception {
	return new Autor();
	}

}
