package br.com.entities.myservices;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import br.com.entities.Livro;
import br.com.entities.LivroDAO;
import br.com.entities.Usuario;
import br.com.exceptions.UsuarioNaoAutorizadoException;

@WebService
//targetNamespace default � o nome do pacote serve para evitar conflito em sistemas")
public class ListagemLivros {
//	<binding name="ListagemLivrosPortBinding" type="tns:ListagemLivros">
	

	/* <operation name="listarLivros">
	<soap:operation soapAction="" />
	<input>
		<soap:body use="literal" />
	</input>
	<output>
		<soap:body use="literal" />
	</output>
    </operation> */
	@WebResult(name = "livro")
	public List<Livro> listarLivros() {
		LivroDAO livroDAO = new LivroDAO();
		return livroDAO.listarLivros();
	}
	 /* dados de entrada e de sa�da do m�todo listarLivros. 
	 <xs:complexType name="listarLivros">
	 <xs:sequence/>
	 </xs:complexType>
	 <xs:complexType name="listarLivrosResponse">
	 <xs:sequence>
	 <xs:element name="livro" type="tns:livro" minOccurs="0"
	 maxOccurs="unbounded"/>
	 </xs:sequence>
	 </xs:complexType> */
	
	/* <ns2:listarLivrosResponse xmlns:ns2="http://myservices.entities.com.br/" xmlns:ns3="http://servicos.estoque.knight.com/excecoes/">
	    <livro>
	       <anoDePublicacao>1955</anoDePublicacao>
	       <editora>Normated</editora>
	       <nome>Casa de Valencia</nome>
	       <resumo>Resumo</resumo>
	       <dataDeCriacao>2015-08-29T17:17:14</dataDeCriacao>
	       <autores>
	          <autor>Fl�vio</autor>
	          <autor>Fl�vio 2</autor>
	       </autores>
	    </livro>
    </ns2:listarLivrosResponse> */


	/* usar sobrecarga de m�todos em web services
	� muito dif�cil.   Existem t�cnicas que podem possibilitar,  mas
	no inal das contas,  acaba sendo trabalhoso demais para resolver um problema
	que n�s poder�amos ter solucionado simplesmente renomeando o m�todo */ 
	/* algumas engines JAX-WS tamb�m t�m problemas comm�todos de nomes iguais,
	ainda que eles estejam em classes diferentes */
	
	// boa pr�tica colocar objetos wrapper como par�metros
	public List<Livro> listarLivrosPaginacao(@WebParam(name="num1")Integer num1,@WebParam(name="num2") Integer num2) {
		LivroDAO livroDAO = new LivroDAO();
		List<Livro> livros = livroDAO.listarLivros();
		int lancaException = num1 + num2;
		livros.get(0).setNome(String.valueOf(num1) + String.valueOf(num2));
		return livros;
	}
	/* <operation name="listarLivrosPaginacao">
	<soap:operation soapAction="" />
	<input>
		<soap:body use="literal" />
	</input>
	<output>
		<soap:body use="literal" />
	</output>
	</operation> */

	public void criarLivro(@WebParam(name = "livro") Livro livro,
			@WebParam(name = "usuario", header = true) Usuario usuario)
			throws UsuarioNaoAutorizadoException, Exception {
		LivroDAO livroDAO = new LivroDAO();
		if (usuario.getLogin() != null &&  usuario.getSenha() !=null && usuario.getLogin().equals("soa")
				&& usuario.getSenha().equals("soa")) {
			livroDAO.criarLivro(livro);
		} else {
			throw new UsuarioNaoAutorizadoException("Usu�rio n�o autorizado TTT");
			/*  <S:Fault xmlns:ns4="http://www.w3.org/2003/05/soap-envelope">
			       <faultcode>S:Server</faultcode>
			       <faultstring>Usu�rio n�o autorizado TTT</faultstring>
			        <detail>
			           <ns3:UsuarioNaoAutorizado mensagem="Usu�rio n�o autorizado TTT" xmlns:ns3="http://servicos.estoque.knight.com/excecoes/" xmlns:ns2="http://myservices.entities.com.br/"/>
			        </detail>
		    </S:Fault> /*
		    
		    <faultcode> Client - problema na requisi��o enviada Server - problema no servidor 
			
			/*SOAPFault soapFault = SOAPFactory.newInstance().createFault(
					"Usu�rio n�o autorizado",
					new QName(SOAPConstants.URI_NS_SOAP_1_1_ENVELOPE,
					"Client.autorizacao"));
					soapFault
					.setFaultActor("http://servicos.estoque.knight.com/LivrosService");
					throw new SOAPFaultException(soapFault);*/
		}
	}
	
	/* <operation name="criarLivro">
			<soap:operation soapAction="" />
			<input>
				<soap:body use="literal" parts="parameters" />
				<soap:header message="tns:criarLivro" part="usuario"
					use="literal" />
			</input>
			<output>
				<soap:body use="literal" />
			</output>
			<fault name="UsuarioNaoAutorizadoException">
				<soap:fault name="UsuarioNaoAutorizadoException" use="literal" />
			</fault>
			<fault name="Exception">
				<soap:fault name="Exception" use="literal" />
			</fault>
		</operation> */
//	</binding>
}
