package br.com.entities;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import javax.xml.ws.http.HTTPException;

public class Autor {

	private String nome;

	private Date dataNascimento;

	public Autor() {

	}

	public Autor(String nome, Date dataNascimento) {
		super();
		this.nome = nome;
		this.dataNascimento = dataNascimento;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	/*
	 * A classe Autor n�o possui nenhuma informa��o a respeito de mapeamento
	 * JAXB. Neste caso, a conigura��o padr�o � utilizar todos os pares de
	 * getters e setters e campos p�blicos. No entanto, o m�todo getRefs n�o
	 * possui um equivalente (ou seja, n�o existe um m�todo setRefs). Assim, o
	 * JAXB n�o ir� considerar este
	 */
	// @XmlElementWrapper(name= "refs" ) - se preferir que for mapeado
    //	@XmlElement(name= "ref" )
	public List<URL> getRefs() throws HTTPException, IOException {
		// Realiza uma busca no Google pelas refer�ncias �quele autor.
		return null;
	}

}
