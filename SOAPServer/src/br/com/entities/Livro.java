package br.com.entities;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import br.com.adapters.AdaptadorAutores;

@XmlAccessorType(XmlAccessType.FIELD)
/* PROPERTY: getters e setters
FIELD: campos das classes seja mmapeados para XML (sejam estes p�blicos ou n�o);
PUBLIC_MEMBER: membros p�blicos (campos ou getters setters sejam mapeados.  � a configura��o padr�o, caso nada
seja informado.
NONE: Esta configura��o faz com que nenhum membro seja mapeado automaticamente,
a n�o ser que o JAXB seja explicitamente informado do contr�rio. */

@XmlSeeAlso({ EBook.class })
// @XmlRootElement(namespace="http://evitarconlitosentresistemas.com.br")
// o namespace default � o nome do pacote
public class Livro {
	// http://localhost:8080/livros?xsd=2
	// <xs:complexType name="livro">
	// <xs:sequence>

	private int anoDePublicacao;
	// <xs:element name="anoDePublicacao" type="xs:int"/>
	private String editora;
	// <xs:element name="editora" type="xs:string" minOccurs="0"/>
	private String nome;
	// <xs:element name="nome" type="xs:string" minOccurs="0"/>
	private String resumo;
	// <xs:element name="resumo" type="xs:string" minOccurs="0"/>
	//@XmlJavaTypeAdapter(AdaptadorDate.class)
	// o adapter est� sendo mapeado pelo pacote - classe package-info
	private Date dataDeCriacao = new Date();
	// <xs:element name="dataDeCriacao" type="xs:anySimpleType" minOccurs="0"/>
	@XmlElementWrapper(name = "autores")
	@XmlElement(name = "autor")
	@XmlJavaTypeAdapter(value = AdaptadorAutores.class)
	// @XmlTransient
	private List<Autor> autores;

	// <xs:element name="autores" minOccurs="0">
	// <xs:complexType>
	// <xs:sequence>
	// <xs:element name="autor" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
	// unbounded - n�o h� limite superior 
	// </xs:sequence>
	// </xs:complexType>
	// </xs:element>

	public int getAnoDePublicacao() {
		return anoDePublicacao;
	}

	public void setAnoDePublicacao(int anoDePublicacao) {
		this.anoDePublicacao = anoDePublicacao;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public Date getDataDeCriacao() {
		return dataDeCriacao;
	}

	public void setDataDeCriacao(Date dataDeCriacao) {
		this.dataDeCriacao = dataDeCriacao;
	}

	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}

	

}
// </xs:sequence>
// </xs:complexType>

