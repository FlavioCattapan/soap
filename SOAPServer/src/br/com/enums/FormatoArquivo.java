package br.com.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "formato")
public enum FormatoArquivo {
// <xs:simpleType name="formato">
// <xs:restriction base="xs:string">
	
	// <xs:enumeration value="pdf"/>
	@XmlEnumValue("pdf")
	PDF, 
	// <xs:enumeration value="mobi"/>
	@XmlEnumValue("mobi")
	MOBI,
	// <xs:enumeration value="EPUB"/>
	@XmlEnumValue("epub")
	EPUB;
	// </xs:restriction>
	// </xs:simpleType>
}
