package br.com.run;
import java.util.List;

import br.com.entities.myservices.ListagemLivros;
import br.com.entities.myservices.ListagemLivrosService;
import br.com.entities.myservices.Livro;


public class Consumer {
	
	
	public static void main(String[] args) {
		
		//Inicia a f�brica dos proxies
		ListagemLivrosService listagemLivrosFactory =
		new ListagemLivrosService();
		//Obt�m um proxy
		ListagemLivros listagemLivros =
		listagemLivrosFactory.getListagemLivrosPort();
		//Executa o m�todo remoto
		List<Livro> livros = listagemLivros.listarLivrosPaginacao(1, 2);
		for (Livro livro : livros) {
		System.out.println("Nome: " + livro.getNome());
		}
		
		


		
	}

}
